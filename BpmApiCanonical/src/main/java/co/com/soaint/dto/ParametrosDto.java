
/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the attributes used in the process.")
public class ParametrosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "Values that represent the process variables.")
	private ValueDto value;
	
	@ApiModelProperty(notes = "List of values that represent the process variables in json.")
	private Map<String, Object> values;

	/**
	 * 
	 */
	public ParametrosDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the values
	 */
	public Map<String, Object> getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(Map<String, Object> values) {
		this.values = values;
	}

	/**
	 * @return the value
	 */
	public ValueDto getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(ValueDto value) {
		this.value = value;
	}
	
}