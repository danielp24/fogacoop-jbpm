/**
 * 
 */
package co.com.soaint.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process container")
public class NodesDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Node name.")
	private String name;

	@ApiModelProperty(notes = "Node id.")
	private String id;

	@ApiModelProperty(notes = "Node Type.")
	private String type;

	@ApiModelProperty(notes = "Node process id")
	private String processId;



	/**
	 *
	 */
	public NodesDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}
}
