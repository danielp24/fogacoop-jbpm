/**
 * 
 */
package co.com.soaint.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process container")
public class ProcessNodesDto implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Node name list.")
	private List<NodesDto> processNode;

	@ApiModelProperty(notes = "Operation response performed by the service.")
	private ResponseDto response;


	/**
	 *
	 */
	public ProcessNodesDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public List<NodesDto> getProcessNode() {
		return processNode;
	}

	public void setProcessNode(List<NodesDto> processNode) {
		this.processNode = processNode;
	}

	public ResponseDto getResponse() {
		return response;
	}

	public void setResponse(ResponseDto response) {
		this.response = response;
	}
}
