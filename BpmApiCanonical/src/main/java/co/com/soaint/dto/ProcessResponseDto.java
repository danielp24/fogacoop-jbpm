/**
 * 
 */
package co.com.soaint.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author ovillamil
 *
 */
@ApiModel(description = "Representative class of the process response.")
public class ProcessResponseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "list of containers created in the engine.")
	private List<ContainerDto> containers;
	
	@ApiModelProperty(notes = "Operation response performed by the service.")
	private ResponseDto response;

	/**
	 * 
	 */
	public ProcessResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the containers
	 */
	public List<ContainerDto> getContainers() {
		return containers;
	}

	/**
	 * @param containers the containers to set
	 */
	public void setContainers(List<ContainerDto> containers) {
		this.containers = containers;
	}

	/**
	 * @return the response
	 */
	public ResponseDto getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(ResponseDto response) {
		this.response = response;
	}
	
}
