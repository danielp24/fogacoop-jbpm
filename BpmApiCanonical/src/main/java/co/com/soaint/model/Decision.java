
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author jjmorales
 *
 */
public class Decision {
	
	@SerializedName("decision-id")
    @Expose
    private String decisionId;
    @SerializedName("decision-name")
    @Expose
    private String decisionName;

    public Decision() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    /**
     * 
     * @return the decisionId
     */
    public String getDecisionId() {
        return decisionId;
    }

    /**
     * 
     * @param decisionId the decisionId to set
     */
    public void setDecisionId(String decisionId) {
        this.decisionId = decisionId;
    }

    /**
     * 
     * @return the decisionName
     */
    public String getDecisionName() {
        return decisionName;
    }

    /**
     * 
     * @param decisionName the decisionName to set
     */
    public void setDecisionName(String decisionName) {
        this.decisionName = decisionName;
    }

}
