
package co.com.soaint.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author jjmorales
 *
 */
public class DmnModelInfoList {
	
	@SerializedName("models")
    @Expose
    private List<Model> models = null;

	public DmnModelInfoList() {
		super();
		// TODO Auto-generated constructor stub
	}
	
    /**
     * 
     * @return the models
     */
    public List<Model> getModels() {
        return models;
    }

    /**
     * 
     * @param models the models to set
     */
    public void setModels(List<Model> models) {
        this.models = models;
    }

}
