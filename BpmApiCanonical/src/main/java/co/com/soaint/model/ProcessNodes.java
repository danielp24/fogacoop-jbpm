
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 
 * @author jjmorales
 *
 */
public class ProcessNodes {


    @SerializedName("process-node")
    @Expose
    private List<Node> decisions = null;


    public ProcessNodes() {
		super();
		// TODO Auto-generated constructor stub
	}

    public List<Node> getDecisions() {
        return decisions;
    }

    public void setDecisions(List<Node> decisions) {
        this.decisions = decisions;
    }
}
