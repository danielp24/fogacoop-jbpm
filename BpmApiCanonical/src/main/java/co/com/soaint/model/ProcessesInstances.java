/**
 * 
 */
package co.com.soaint.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ovillamil
 *
 */
public class ProcessesInstances {

	@SerializedName("process-instance")
	@Expose
	private List<KieProcess> processInstance;

	/**
	 * 
	 */
	public ProcessesInstances() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the processInstance
	 */
	public List<KieProcess> getProcessInstance() {
		return processInstance;
	}

	/**
	 * @param processInstance the processInstance to set
	 */
	public void setProcessInstance(List<KieProcess> processInstance) {
		this.processInstance = processInstance;
	}
	
}
