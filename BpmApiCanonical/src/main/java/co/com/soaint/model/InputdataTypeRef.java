
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InputdataTypeRef {
	
	@SerializedName("namespace-uri")
    @Expose
    private String namespaceUri;
    @SerializedName("local-part")
    @Expose
    private String localPart;
    @SerializedName("prefix")
    @Expose
    private String prefix;

    public InputdataTypeRef() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    public String getNamespaceUri() {
        return namespaceUri;
    }

    public void setNamespaceUri(String namespaceUri) {
        this.namespaceUri = namespaceUri;
    }

    public String getLocalPart() {
        return localPart;
    }

    public void setLocalPart(String localPart) {
        this.localPart = localPart;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

}
