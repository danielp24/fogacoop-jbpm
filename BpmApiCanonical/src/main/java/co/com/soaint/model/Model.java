
package co.com.soaint.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author jjmorales
 *
 */
public class Model {
	
	@SerializedName("model-namespace")
    @Expose
    private String modelNamespace;
    @SerializedName("model-name")
    @Expose
    private String modelName;
    @SerializedName("model-id")
    @Expose
    private String modelId;
    @SerializedName("decisions")
    @Expose
    private List<Decision> decisions = null;
    @SerializedName("inputs")
    @Expose
    private List<Input> inputs = null;
    @SerializedName("itemDefinitions")
    @Expose
    private List<Object> itemDefinitions = null;
    @SerializedName("decisionServices")
    @Expose
    private List<Object> decisionServices = null;

    public Model() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    /**
     * 
     * @return the modelNamespace
     */
    public String getModelNamespace() {
        return modelNamespace;
    }

    /**
     * 
     * @param modelNamespace the modelNamespace to set
     */
    public void setModelNamespace(String modelNamespace) {
        this.modelNamespace = modelNamespace;
    }

    /**
     * 
     * @return the modelName
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * 
     * @param modelName the modelName to set
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * 
     * @return the modelId
     */
    public String getModelId() {
        return modelId;
    }

    /**
     * 
     * @param modelId the modelId to set
     */
    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    /**
     * 
     * @return the decisions
     */
    public List<Decision> getDecisions() {
        return decisions;
    }

    /**
     * 
     * @param decisions the decisions to set
     */
    public void setDecisions(List<Decision> decisions) {
        this.decisions = decisions;
    }

    /**
     * 
     * @return the inputs
     */
    public List<Input> getInputs() {
        return inputs;
    }

    /**
     * 
     * @param inputs the inputs to set
     */
    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }

    /**
     * 
     * @return the itemDefinitions
     */
    public List<Object> getItemDefinitions() {
        return itemDefinitions;
    }

    /**
     * 
     * @param itemDefinitions the itemDefinitions to set
     */
    public void setItemDefinitions(List<Object> itemDefinitions) {
        this.itemDefinitions = itemDefinitions;
    }

    /**
     * 
     * @return the decisionServices
     */
    public List<Object> getDecisionServices() {
        return decisionServices;
    }

    /**
     * 
     * @param decisionServices the decisionServices to set
     */
    public void setDecisionServices(List<Object> decisionServices) {
        this.decisionServices = decisionServices;
    }

}
