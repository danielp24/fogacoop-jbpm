
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author jjmorales
 *
 */
public class Result {

    @SerializedName("kie-containers")
    @Expose
    private KieContainers kieContainers;
    
    @SerializedName("dmn-model-info-list")
    @Expose
    private DmnModelInfoList dmnModelInfoList;
    
    @SerializedName("dmn-evaluation-result")
    @Expose
    private DmnEvaluationResult dmnEvaluationResult;

	/**
	 * 
	 */
	public Result() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the kieContainers
	 */
	public KieContainers getKieContainers() {
		return kieContainers;
	}

	/**
	 * @param kieContainers the kieContainers to set
	 */
	public void setKieContainers(KieContainers kieContainers) {
		this.kieContainers = kieContainers;
	}
	
	/**
	 * 
	 * @return the dmnModelInfoList
	 */
	public DmnModelInfoList getDmnModelInfoList() {
        return dmnModelInfoList;
    }

	/**
	 * 
	 * @param dmnModelInfoList the dmnModelInfoList to set
	 */
    public void setDmnModelInfoList(DmnModelInfoList dmnModelInfoList) {
        this.dmnModelInfoList = dmnModelInfoList;
    }
    
    /**
     * 
     * @return the dmnEvaluationResult
     */
    public DmnEvaluationResult getDmnEvaluationResult() {
        return dmnEvaluationResult;
    }

    /**
     * 
     * @param dmnEvaluationResult the dmnEvaluationResult to set
     */
    public void setDmnEvaluationResult(DmnEvaluationResult dmnEvaluationResult) {
        this.dmnEvaluationResult = dmnEvaluationResult;
    }

}
