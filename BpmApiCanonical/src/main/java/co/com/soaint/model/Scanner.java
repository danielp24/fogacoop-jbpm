
package co.com.soaint.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Scanner {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("poll-interval")
    @Expose
    private Object pollInterval;
    
	/**
	 * 
	 */
	public Scanner() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the pollInterval
	 */
	public Object getPollInterval() {
		return pollInterval;
	}

	/**
	 * @param pollInterval the pollInterval to set
	 */
	public void setPollInterval(Object pollInterval) {
		this.pollInterval = pollInterval;
	}

}
