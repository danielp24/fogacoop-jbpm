/**
 * 
 */
package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.UserDto;
import co.com.soaint.service.ContainerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@RequestMapping("/api/container")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the list of containers associated with each project")
public class ContainerController {
	
	@Autowired
	ContainerService service;
	
	/**
	 * Get All containers
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/containers", method = RequestMethod.POST)
	@ApiOperation("Returns list of all containers by project")
    public ResponseEntity<ProcessResponseDto> listAllContainers (@RequestBody ProcessRequestDto requestDto) {
        if (service.getContainers(requestDto) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getContainers(requestDto), HttpStatus.OK);
    }
}
