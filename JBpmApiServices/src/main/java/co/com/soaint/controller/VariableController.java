
/**
 * 
 */
package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.VariableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@RequestMapping("/api/variable")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the process variables and their values.")
public class VariableController {

	@Autowired
	VariableService service;
	
	/**
	 * Get the values of the process variables
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/variableValuesByProcess", method = RequestMethod.POST)
	@ApiOperation("Returns the values of the process variables.")
    public ResponseEntity<ProcessResponseDto> variableValuesByProcess(@RequestBody ProcessRequestDto request) {
        if (service.getValuesProcessVariables(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getValuesProcessVariables(request), HttpStatus.OK);
    }
	
	/**
	 * Get all variables by process instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/variablesByProces", method = RequestMethod.POST)
	@ApiOperation("Returns list of all variables by process instance.")
    public ResponseEntity<ProcessResponseDto> variablesByProcessInstance(@RequestBody ProcessRequestDto request) {
        if (service.getProcessVariables(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getProcessVariables(request), HttpStatus.OK);
    }
	
	/**
	 * Get list of tasks by variable
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/tasksByVariable", method = RequestMethod.POST)
	@ApiOperation("Returns list of all active tasks by variable.")
    public ResponseEntity<ProcessResponseDto> listAllTaskByVariable (@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = service.getTasksByVariable(requestDto);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Get list of instances by variable
	 * @param requestDto
	 * @return
	 */
	@RequestMapping(value = "/instancesByVariable", method = RequestMethod.POST)
	@ApiOperation("Returns list of all active tasks by variable.")
    public ResponseEntity<ProcessResponseDto> listAllInstancesByVariable (@RequestBody ProcessRequestDto requestDto) {
		ProcessResponseDto processResponseDto = service.getInstancesByVariable(requestDto);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
}
