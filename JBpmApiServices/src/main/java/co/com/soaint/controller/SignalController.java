/**
 * 
 */
package co.com.soaint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.SignalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author ovillamil
 *
 */
@RestController
@RequestMapping("/api/signal")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Control of the signals generated in the life cycle of the process.")
public class SignalController {

	@Autowired
	SignalService service;
	
	/**
	 * Send signals active process instances
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/processSignal", method = RequestMethod.POST)
	@ApiOperation("Send signals active process instances.")
    public ResponseEntity<ProcessResponseDto> sendProcessSignal(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto response = service.sendSignaByProcess(request);
        if ( response == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(response, HttpStatus.OK);
    }
}
