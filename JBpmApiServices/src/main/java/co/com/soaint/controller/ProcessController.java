
/**
 * 
 */
package co.com.soaint.controller;

import co.com.soaint.dto.NodesDto;
import co.com.soaint.dto.ProcessNodesDto;
import co.com.soaint.model.ProcessesInstances;
import co.com.soaint.trasnformData.ProcessData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.service.ProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import java.util.Map;

/**
 * @author ovillamil
 *
 */
@RestController
@RequestMapping("/api/process")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(description = "Controls the list of processes per container and creates a new process instance")
public class ProcessController {
	
	@Autowired
	ProcessService service;

	@Autowired
	private Environment environment;

	@Autowired
	private ProcessData processData;

	/**
	 * Get all process by container
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/processesByContainer", method = RequestMethod.POST)
	@ApiOperation("Returns list of all process by container.")
    public ResponseEntity<ProcessResponseDto> allProcessByContainer(@RequestBody ProcessRequestDto request) {
        if (service.getProcessByContainerId(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getProcessByContainerId(request), HttpStatus.OK);
    }
	
	/**
	 * Create new Process Instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/startProcessInstance", method = RequestMethod.POST)
	@ApiOperation("Create new Process Instance in the container.")
    public ResponseEntity<ProcessResponseDto> addProcessInstance (@RequestBody ProcessRequestDto request) {
		String processInstance = service.startProcessInstance(request);
        if (processInstance == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        } 
        request.setProcessInstance(processInstance);
        return new ResponseEntity<ProcessResponseDto>(service.getPropertiesByProcessInstance(request), HttpStatus.OK);
    }
	
	/**
	 * Get all process Instances By Container
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/intancesByContainer", method = RequestMethod.POST)
	@ApiOperation("Returns list of all process instance by container.")
    public ResponseEntity<ProcessResponseDto> allProcessInstanceByContainer(@RequestBody ProcessRequestDto request) {
        if (service.getProcessInstanceByContainer(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.getProcessInstanceByContainer(request), HttpStatus.OK);
    }
	
	/**
	 * Get all process definitions
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/listDefinitions", method = RequestMethod.POST)
	@ApiOperation("Returns all process definitions.")
    public ResponseEntity<ProcessResponseDto> allProcessDefinitions(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto processResponseDto = service.getProcessDefinitions(request);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	/**
	 * Cancel the active process instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/processInstance", method = RequestMethod.DELETE)
	@ApiOperation("Cancel the active process instance.")
    public ResponseEntity<ProcessResponseDto> cancelProcessIntance(@RequestBody ProcessRequestDto request) {
        if (service.cancelProcessInstance(request) == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(service.cancelProcessInstance(request), HttpStatus.OK);
    }

	/**
	 * terminate the active process instance
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/terminateProcessInstance", method = RequestMethod.DELETE)
	@ApiOperation("Teminated Process instance By node name.")
    public ResponseEntity<ProcessResponseDto> terminateProccesInstance(@RequestBody ProcessRequestDto request) {
        ProcessNodesDto response = service.getNodesActiveInstance(request);
		ProcessResponseDto resRet = null;
		if(response.getProcessNode().size()>0){
			request.setNodeNameEndProcess((request.getNodeNameEndProcess() == null) ? environment.getProperty("nodeNameProcessTerminate").toString() : request.getNodeNameEndProcess());
			List<NodesDto> nodeSelected = processData.findNodeInList(response.getProcessNode(),request.getNodeNameEndProcess());
			if(nodeSelected.size()>0){
				resRet = service.triggerNode(request,nodeSelected.get(0));
				return new ResponseEntity<ProcessResponseDto>(resRet, HttpStatus.OK);
			}
		}
        return new ResponseEntity<ProcessResponseDto>(resRet, HttpStatus.BAD_REQUEST);
    }
	
	/**
	 * Get all process Instances
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/allInstances", method = RequestMethod.POST)
	@ApiOperation("Returns all process instances.")
    public ResponseEntity<ProcessResponseDto> allProcessInstances(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto processResponseDto = service.getProcessInstances(request);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }
	
	
	/**
	 * Get all process Instances By Container and Status 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/allInstancesByStatus", method = RequestMethod.POST)
	@ApiOperation("Returns all process instances by status.")
    public ResponseEntity<ProcessResponseDto> allProcessInstancesByStatus(@RequestBody ProcessRequestDto request) {
		ProcessResponseDto processResponseDto = service.getProcessInstancesByStatus(request);
        if (processResponseDto == null) {
            return new ResponseEntity<ProcessResponseDto>(HttpStatus.NO_CONTENT);
        }         
        return new ResponseEntity<ProcessResponseDto>(processResponseDto, HttpStatus.OK);
    }


	/**
	 * Get all Subprocess By Process ID
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/allSubProcessByProcess", method = RequestMethod.POST)
	@ApiOperation("Returns all process instances by status.")
	public ResponseEntity<ProcessesInstances> allSubProcessByProcess(@RequestBody ProcessRequestDto request) {
		ProcessesInstances processResponseDto = service.getAllSubProcessByProcess(request);
		if (processResponseDto == null) {
			return new ResponseEntity<ProcessesInstances>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<ProcessesInstances>(processResponseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getInstanceByProcessId", method = RequestMethod.POST)
	@ApiOperation("Returns instance process by id.")
	public ResponseEntity<Map<String, Object>> getInstanceByProcessId(@RequestBody ProcessRequestDto request) {
		Map<String,Object> processResponseDto = service.getInstanceByProcessId(request);
		if (processResponseDto == null) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Map<String, Object>>(processResponseDto, HttpStatus.OK);
	}

}
