package co.com.soaint.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.model.Response;
import co.com.soaint.model.TaskResponse;
import co.com.soaint.trasnformData.RuleData;

/**
 * 
 * @author jjmorales
 *
 */
@Service
public class RuleService {
	
	@Autowired
	private Environment environment;

	@Autowired
	private RuleData ruleData;

	private RestTemplate restTemplate;

	/**
	 * 
	 */
	public RuleService() {
		restTemplate = new RestTemplate();
	}

	/**
	 * 
	 * @param processRequestDto
	 * @return
	 */
	public ProcessResponseDto getRules(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+processRequestDto.getContainerId()
		+"/dmn/";		
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);		
		HttpEntity<?> request = new HttpEntity<>(headers);		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		Response response = new Gson().fromJson(postResponse.getBody(), Response.class);		
        return ruleData.kieResponseToResponseDto(response);
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto evaluateRule(ProcessRequestDto processRequestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+processRequestDto.getContainerId()
		+"/dmn/";
		String user = processRequestDto.getOwnerUser().getUser();
		String password = processRequestDto.getOwnerUser().getPassword();
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		Map<String, Object> map = new HashMap<String, Object>(); 
		if (processRequestDto.getParametros() != null && processRequestDto.getParametros().getValues() != null) {
			map = processRequestDto.getParametros().getValues();
		}
		HttpEntity<?> request = new HttpEntity<>(map, headers);
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		Response response = new Gson().fromJson(postResponse.getBody(), Response.class);
		
        return ruleData.kieResponseToResponseDto(response);
	}

}
