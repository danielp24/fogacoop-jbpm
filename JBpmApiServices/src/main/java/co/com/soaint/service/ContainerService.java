/**
 * 
 */
package co.com.soaint.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.model.Response;
import co.com.soaint.trasnformData.ContainerData;

/**
 * @author ovillamil
 *
 */
@Service
public class ContainerService {

	@Autowired
	private Environment environment;
	
	@Autowired
	private ContainerData containerData;
	
	private RestTemplate restTemplate;
	
	/**
	 * 
	 */
	public ContainerService(){
		restTemplate = new RestTemplate();
	}
	
	/**
	 * Consultar Lista de Contenedores
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getContainers (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		Response response = new Gson().fromJson(postResponse.getBody(), Response.class);
		
        return containerData.kieResponseToResponseDto(response);
	}
	 
}
