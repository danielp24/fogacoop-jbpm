
/**
 * 
 */
package co.com.soaint.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.com.soaint.dto.NodesDto;
import co.com.soaint.dto.ProcessNodesDto;
import co.com.soaint.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.trasnformData.ProcessData;

/**
 * @author ovillamil
 *
 */
@Service
public class ProcessService {

	@Autowired
	private Environment environment;
	
	@Autowired
	private ProcessData processData;
	
	private RestTemplate restTemplate;
	
	/**
	 * 
	 */
	public ProcessService(){
		restTemplate = new RestTemplate();
	}
	
	/**
	 * Search all process by container
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessByContainerId (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/" + requestDto.getContainerId() + "/processes";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		KieProcesses response = new Gson().fromJson(postResponse.getBody(), KieProcesses.class);
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * Create Process Instance
	 * @param requestDto
	 * @return
	 */
	public String startProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/" + requestDto.getProcessesId() + "/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
					
		Map<String, Object> map = new HashMap<String, Object>(); 
		if (requestDto.getParametros() != null && requestDto.getParametros().getValues() != null) {
			map = requestDto.getParametros().getValues();
		}
		
		HttpEntity<?> request = new HttpEntity<>(map, headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
		
		return postResponse.getBody();
	}
	
	/**
	 * Return Properties to process instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getPropertiesByProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/instances/" + requestDto.getProcessInstance();
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		KieProcess kieProcess = new Gson().fromJson(postResponse.getBody(), KieProcess.class);
		
		List<KieProcess> processes = new ArrayList<KieProcess>();
						 processes.add(kieProcess);
		KieProcesses response = new KieProcesses();
					 response.setProcesses(processes);
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * Search all Instances by container
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessInstanceByContainer (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + 
					"/containers/" + requestDto.getContainerId() + "/processes/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);
		
		KieProcesses response = new KieProcesses();
					 response.setProcesses(instances.getProcessInstance());
		
        return processData.kieProcessesToResponseDto(response, requestDto.getContainerId());
	} 
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessDefinitions (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/processes/definitions";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		
		KieProcesses response = new Gson().fromJson(postResponse.getBody(), KieProcesses.class);
		
        return processData.kieProcessesToResponseDto(response);
	} 
	
	/**
	 * Cancel active Process Instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto cancelProcessInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() 
				+ "/containers/" + requestDto.getContainerId() + "/processes/instances";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
  															  .queryParam("instanceId", requestDto.getProcessInstance());
		
		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.DELETE, request, String.class);
		
        return processData.responseToResponseDto(postResponse);
	}

	/**
	 * Retunr all nodes from active instance
	 * @param requestDto
	 * @return
	 */
	public ProcessNodesDto getNodesActiveInstance (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString()
				+ "/admin/containers/"+requestDto.getContainerId()+"/processes/instances/"
				+requestDto.getProcessInstance()+"/nodes";
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();

		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);

		HttpEntity<?> request = new HttpEntity<>(headers);


		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		ProcessNodes response = new Gson().fromJson(postResponse.getBody(), ProcessNodes.class);
        return processData.processNodesToProcessNodesDto(response);
	}


	/**
	 * Retunr all nodes from active instance
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto triggerNode (ProcessRequestDto requestDto, NodesDto node) {
		String url = environment.getProperty("jbpm_end_point").toString()
				+ "/admin/containers/"+requestDto.getContainerId()+"/processes/instances/"
				+requestDto.getProcessInstance()+"/nodes/"+node.getId();
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();

		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);

		HttpEntity<?> request = new HttpEntity<>(headers);


		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.POST, request, String.class);

        return processData.responseToResponseDto(postResponse);
	}

	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessInstances(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/queries/processes/instances";
			
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);		
		
		ResponseEntity<String> postResponse = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
		ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);
		
		KieProcesses response = new KieProcesses();
					 response.setProcesses(instances.getProcessInstance());
		
        return processData.kieProcessesToResponseDto(response);
	} 
	
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto getProcessInstancesByStatus(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+requestDto.getContainerId()+
				"/processes/instances";
		
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth(user, password);
		
		HttpEntity<?> request = new HttpEntity<>(headers);		
		UriComponentsBuilder uriBuilder = loadUriBuilder(requestDto, url);

		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
		ProcessesInstances instances = new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);		
		
		KieProcesses response = new KieProcesses();
		response.setProcesses(instances.getProcessInstance());
		
		return processData.kieProcessesToResponseDto(response);
	} 
	
	/**
	 * 
	 * @param requestDto
	 * @return
	 */
	private UriComponentsBuilder loadUriBuilder(ProcessRequestDto requestDto, String url) {
		UriComponentsBuilder uriBuilder = null;
		final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		if(requestDto.getInstanceStates()!=null && requestDto.getInstanceStates().size()>0)
			params.put("status", requestDto.getInstanceStates());
		
		uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParams(params);
		return uriBuilder;
	}


	/**
	 *
	 * @param requestDto
	 * @return
	 */
	public ProcessesInstances getAllSubProcessByProcess(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+requestDto.getContainerId()+
				"/processes/instances/"+requestDto.getProcessInstance()+"/processes";

		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth(user, password);

		HttpEntity<?> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = loadUriBuilder(requestDto, url);

		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
		return new Gson().fromJson(postResponse.getBody(), ProcessesInstances.class);

	}

	public Map<String, Object> getInstanceByProcessId(ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/"+requestDto.getContainerId()+
				"/processes/instances/"+requestDto.getProcessesId();

		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBasicAuth(user, password);

		HttpEntity<?> request = new HttpEntity<>(headers);
		UriComponentsBuilder uriBuilder = loadUriBuilder(requestDto, url);

		ResponseEntity<String> postResponse = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, request, String.class);
		return new Gson().fromJson(postResponse.getBody(), Map.class);

	}
}