/**
 * 
 */
package co.com.soaint.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import co.com.soaint.dto.ProcessRequestDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.trasnformData.SignalData;

/**
 * @author ovillamil
 * @author jmedina
 */
@Service
public class SignalService {
	
	@Autowired
	private Environment environment;
	
	@Autowired
	private SignalData signalData;
	
	private RestTemplate restTemplate;
	
	/**
	 * 
	 */
	public SignalService(){
		restTemplate = new RestTemplate();
	}
	
	/**
	 * Send signal to any process
	 * @param requestDto
	 * @return
	 */
	public ProcessResponseDto sendSignaByProcess (ProcessRequestDto requestDto) {
		String url = environment.getProperty("jbpm_end_point").toString() + "/containers/" + requestDto.getContainerId()
			+ "/processes/instances/signal/" + requestDto.getSignal().getSignalName();
		String user = requestDto.getOwnerUser().getUser();
		String password = requestDto.getOwnerUser().getPassword();
		
		HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					headers.setBasicAuth(user, password);
		
		Map<String, Object> map = new HashMap<String, Object>(); 
		if (requestDto.getSignal().getParametros() != null && requestDto.getSignal().getParametros().getValues() != null) {
			map = requestDto.getSignal().getParametros().getValues();
		}
		
		HttpEntity<?> request = new HttpEntity<>(map, headers);
		
		UriComponentsBuilder uriBuilder = null;
		if(requestDto.getProcessInstance() != null &&  !"".equalsIgnoreCase(requestDto.getProcessInstance())){
			uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
					.queryParam("instanceId", requestDto.getProcessInstance());
		}else {
			uriBuilder = UriComponentsBuilder.fromHttpUrl(url);
		}
		
		ResponseEntity<String> postResponse = restTemplate.postForEntity(uriBuilder.toUriString(), request, String.class);
		
        return signalData.kieResponseToResponseDto(postResponse);
	} 

}
