/**
 * 
 */
package co.com.soaint.trasnformData;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;

/**
 * @author ovillamil
 *
 */
@Service
public class SignalData {

	/**
	 * Response services to Response Dto
	 * @param postResponse
	 * @return
	 */
	public ProcessResponseDto kieResponseToResponseDto(ResponseEntity<String> postResponse) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		// Asignación de Respuesta
    	ResponseDto responseDto = new ResponseDto();
    				responseDto.setStatusCode(Integer.toString(postResponse.getStatusCodeValue()));
    				responseDto.setMsg("SUCCESS");
    				responseDto.setType("SUCCESS");
    	processResponseDto.setResponse(responseDto);
		
		return processResponseDto;
	}
}
