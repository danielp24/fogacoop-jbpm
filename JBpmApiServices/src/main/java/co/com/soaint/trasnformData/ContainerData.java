/**
 * 
 */
package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import co.com.soaint.dto.ContainerDto;
import co.com.soaint.dto.MessagesDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.model.KieContainer;
import co.com.soaint.model.Message;
import co.com.soaint.model.Response;

/**
 * @author ovillamil
 *
 */
@Service
public class ContainerData {
	
	public ContainerData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Trasforma Kie Data to dto 
	 * @param response
	 * @param dto
	 * @return
	 */
	public ProcessResponseDto kieResponseToResponseDto (Response response) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
						   
		if (response.getType().equals("SUCCESS") && response.getResult().getKieContainers() != null) {
        	List<ContainerDto> containers = new ArrayList<ContainerDto>();
        	
        	// Logica para el contenedor
        	if (response.getResult().getKieContainers() != null && response.getResult().getKieContainers().getKieContainer() != null) {
	        	for (KieContainer e : response.getResult().getKieContainers().getKieContainer()) {
					ContainerDto container = new ContainerDto();
									 container.setContainerId(e.getContainerId());
									 container.setVersion(e.getReleaseId().getVersion());
									 container.setStatus(e.getStatus());
									 container.setContainerAlias(e.getContainerAlias());
									
					// Lista de mensajes de Respuesta
					List<MessagesDto> messages = new ArrayList<MessagesDto>();
					for (Message m : e.getMessages()) {
						MessagesDto message = new MessagesDto();
									message.setContent(m.getContent());
									message.setFecha(m.getTimestamp().getJavaUtilDate());
									message.setSeverity(m.getSeverity());
						messages.add(message);
					}
					container.setMessages(messages);
					containers.add(container);
	        	}
	        	processResponseDto.setContainers(containers);
        	}
        	
        	// Asignación de Respuesta
        	ResponseDto responseDto = new ResponseDto();
        				responseDto.setMsg(response.getMsg());
        				responseDto.setType(response.getType());
        				responseDto.setStatusCode("200");
        	processResponseDto.setResponse(responseDto);
        }
		
		return processResponseDto;
	}

}
