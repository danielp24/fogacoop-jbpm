/**
 * 
 */
package co.com.soaint.trasnformData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import co.com.soaint.dto.ContainerDto;
import co.com.soaint.dto.InstanceDto;
import co.com.soaint.dto.ProcessResponseDto;
import co.com.soaint.dto.ProcessesDto;
import co.com.soaint.dto.ResponseDto;
import co.com.soaint.dto.TaskDto;
import co.com.soaint.model.TaskResponse;
import co.com.soaint.model.TaskSummary;

/**
 * @author jjmorales
 *
 */
@Service
public class TaskData {

	public TaskData() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Transform Kie Data to DTO
	 * @param response
	 * @return
	 */
	public ProcessResponseDto kieResponseToResponseDto(TaskResponse response) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();

		if (!response.getTaskSummary().isEmpty()) {
			List<ContainerDto> containers = new ArrayList<ContainerDto>();
			ContainerDto containerDto = new ContainerDto();
			List<ProcessesDto> processes = new ArrayList<ProcessesDto>();
			ProcessesDto processesDto = new ProcessesDto();
			InstanceDto instanceDto = new InstanceDto();
			List<TaskDto> tasks = new ArrayList<TaskDto>();
			TaskDto taskDto = new TaskDto();
			
			Iterator<TaskSummary> it = response.getTaskSummary().iterator();
			while (it.hasNext()) {
				TaskSummary taskSummary= it.next();
				
				taskDto = new TaskDto();
					taskDto.setTaskId(taskSummary.getTaskId());
					taskDto.setTaskName(taskSummary.getTaskName());
					taskDto.setTaskSubject(taskSummary.getTaskSubject() != null ? taskSummary.getTaskSubject().toString() : null);
					taskDto.setTaskDescription(taskSummary.getTaskDescription());
					taskDto.setTaskStatus(taskSummary.getTaskStatus());
					taskDto.setTaskPriority(taskSummary.getTaskPriority());
					taskDto.setTaskActualOwner(taskSummary.getTaskActualOwner());
					taskDto.setTaskCreatedBy(taskSummary.getTaskCreatedBy());
					taskDto.setTaskCreatedOn(taskSummary.getTaskCreatedOn().getJavaUtilDate().toString());
					taskDto.setTaskActivationTime(taskSummary.getTaskActivationTime().getJavaUtilDate().toString());
					taskDto.setTaskExpirationTime(taskSummary.getTaskExpirationTime() != null ? taskSummary.getTaskExpirationTime().toString() : null);
					taskDto.setTaskProcDefId(taskSummary.getTaskProcDefId() != null ? taskSummary.getTaskProcDefId() : taskSummary.getTaskProcessId());	
					taskDto.setContainerId(taskSummary.getTaskContainerId());
					taskDto.setInstanceId(taskSummary.getTaskProcInstId() != null ? taskSummary.getTaskProcInstId().toString() : taskSummary.getTaskProcessInstanceId().toString());
				tasks.add(taskDto);
			}
			
			processesDto.setTaskList(tasks);
			processesDto.setInstance(instanceDto);
			processes.add(processesDto);
			containerDto.setProcesses(processes);
			containers.add(containerDto);
			processResponseDto.setContainers(containers);

			// Asignación de Respuesta
			ResponseDto responseDto = new ResponseDto();
						responseDto.setStatusCode("200");
						responseDto.setMsg("SUCCESS");
        				responseDto.setType("SUCCESS");
			processResponseDto.setResponse(responseDto);
		}
		return processResponseDto;
	}
	
	/**
	 * Transform Kie Data to DTO
	 * @param e
	 * @return
	 */
	public ProcessResponseDto kieResponseToResponseDto(Exception e) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		// Asignación de Respuesta
		ResponseDto responseDto = new ResponseDto();
		
		String sAux = e.getMessage();
		String[] parts = sAux.split(" ");
		
		if(parts.length>0) {
			responseDto.setStatusCode(parts[0]);
		}else {
			responseDto.setStatusCode("404");
		}
		responseDto.setMsg(e.toString());
		responseDto.setType("FAIL");
    	processResponseDto.setResponse(responseDto);
		return processResponseDto;
	}

	/**
	 * Transform Kie Data to DTO
	 * @param postResponse
	 * @return
	 */
	public ProcessResponseDto kieResponseToResponseDto(ResponseEntity<String> postResponse) {
		ProcessResponseDto processResponseDto = new ProcessResponseDto();
		
		// Asignación de Respuesta
    	ResponseDto responseDto = new ResponseDto();
    				responseDto.setStatusCode(Integer.toString(postResponse.getStatusCodeValue()));
    				responseDto.setMsg("SUCCESS");
    				responseDto.setType("SUCCESS");
    	processResponseDto.setResponse(responseDto);
		
		return processResponseDto;
	}
}
